const express = require("express");
const router = express.Router();
const { getAllPeople, createPeople, updatePeople, deletePeople } = require("./../controllers/people");

// router.get("/", getAllPeople);

// router.post("/", createPeople);

// router.post("/postman", createPeople);

// router.put("/:id", updatePeople);

// router.delete("/:id", deletePeople);

router.route("/").get(getAllPeople).post(createPeople);
router.route("/postman").post(createPeople);
router.route("/:id").put(updatePeople).delete(deletePeople);

module.exports = router;

const express = require("express");
const app = express();

//setup middleware using third party

const morgan = require("morgan");

app.use(morgan("tiny"));

app.get("/", (req, res) => {
    return res.status(200).send("Home");
});

app.get("/about", (req, res) => {
    return res.send("About");
});

app.get("/api.products", (req, res) => {
    return res.send("Products");
});

app.get("/api/items", (req, res) => {
    return res.send("Items");
});

app.listen(5000, (err) => {
    if (err) {
        console.log("Error : ", err);
        return;
    }
    console.log("Server is listening on port : 5000");
});

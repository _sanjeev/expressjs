const express = require("express");
const app = express();

const { products, people } = require("./data");

app.get("/", (req, res) => {
    return res.status(200).json(products);
});

app.listen(5000, (err) => {
    if (err) {
        console.log("Error : ", err);
        return;
    }
    console.log("Server is listening on port : 5000");
});

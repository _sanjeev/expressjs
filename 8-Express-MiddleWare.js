const express = require("express");
const logger = require("./logger");

const app = express();

//if you don't apply any path it works for all the request
app.use(logger);

//If you use some path it works only when base path starts with api
// app.use("/api", logger);

app.get("/", (req, res) => {
    return res.send("Home");
});

app.get("/about", (req, res) => {
    return res.send("About");
});

app.get("/api/products", (req, res) => {
    return res.send("Products");
});

app.get("/api/items", (req, res) => {
    return res.send("Items");
});

app.listen(5000, (err) => {
    if (err) {
        console.log("Error : ", err);
        return;
    }
    console.log("Sever is listening on port : 5000");
});

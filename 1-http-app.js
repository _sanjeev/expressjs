const http = require("http");
const { readFileSync } = require("fs");

const homePage = readFileSync("./index.html");
const homeStyle = readFileSync("./styles.css");
const homeLogo = readFileSync("./logo.svg");

const server = http.createServer((req, res) => {
    if (req.url === "/") {
        res.writeHead(200, {
            "content-type": "text/html",
        });
        return res.end(homePage);
    }
    if (req.url === "/styles.css") {
        res.writeHead(200, {
            "content-type": "text/css",
        });
        return res.end(homeStyle);
    }
    if (req.url === "/logo.svg") {
        res.writeHead(200, {
            "content-type": "image/svg+xml",
        });
        return res.end(homeLogo);
    }
    res.writeHead(404, {
        "content-type": "text/html",
    });
    return res.end("<h1>Oops! 404-Not found</h1>");
});

server.listen(5000, (err) => {
    if (err) {
        console.log("Error", err);
        return;
    }
    console.log("Server is listening on port : 5000");
});

const http = require("http");

const server = http.createServer((req, res) => {
    // console.log(req.method);
    // console.log(req.url);
    //Home page
    if (req.url === "/") {
        res.writeHead(200, {
            "content-type": "text/html",
        });
        res.write("<h1>Welcome to home page.</h1>");
        return res.end();
    }
    //about page
    else if (req.url === "/about") {
        res.writeHead(200, {
            "content-type": "text/html",
        });
        res.write("<h1>Welcome to about page</h1>");
        return res.end();
    }
    // contact-us page
    else if (req.url === "/contact-us") {
        res.writeHead(200, {
            "content-type": "text/html",
        });
        res.write("<h1>Welcome to contact us page</h1>");
        return res.end();
    }
    //404 Not found
    else {
        res.writeHead(404, {
            "content-type": "text/html",
        });
        res.write("<h1>404: Not found!</h1>");
        return res.end();
    }
});

server.listen(5000, (err) => {
    if (err) {
        console.log("Error : ", err);
        return;
    }
    console.log("Server is listening on port : 5000");
});

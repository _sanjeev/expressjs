let { people } = require("../data");

module.exports.getAllPeople = (req, res) => {
    return res.status(200).json({
        status: true,
        entity: people,
    });
};

module.exports.createPeople = (req, res) => {
    const { id, name } = req.body;
    if (!name) {
        return res.status(400).json({
            status: false,
            message: "Please provide a name value",
        });
    }
    people.push({ id, name });
    return res.status(201).json({
        status: true,
        data: name,
    });
};

module.exports.updatePeople = (req, res) => {
    const { name } = req.body;
    const { id } = req.params;
    if (!name) {
        return res.status(400).send({
            status: false,
            message: "Please provide a name value",
        });
    }
    const person = people.find((person) => person.id === Number(id));

    if (!person) {
        return res.status(404).json({ status: false, message: `No person with ${id} is present` });
    }
    const newPerson = people.map((person) => {
        if (person.id === Number(id)) {
            person.name = name;
        }
        return person;
    });
    return res.status(200).json({ status: true, data: newPerson });
};

module.exports.deletePeople = (req, res) => {
    const { id } = req.params;
    const person = people.find((key) => key.id === Number(id));
    if (!person) {
        return res.status(404).json({ status: false, message: "Resource not found!" });
    }
    const newPerson = people.filter((key) => key.id !== Number(id));
    return res.status(200).json({ status: true, message: "Successfully deleted the people", data: newPerson });
};

const express = require("express");
const path = require("path");
const app = express();

//setup static and middleware
app.use(express.static("public"));

// app.get("/", (req, res) => {
//     return res.status(200).sendFile(path.resolve(__dirname, "index.html"));
// });

app.all("*", (req, res) => {
    return res.status(404).send("Resource not found!");
});

app.listen(5000, (err) => {
    if (err) {
        console.log("Error : ", err);
        return;
    }
    console.log("Server is listening on port : 5000");
});

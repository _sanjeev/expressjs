const express = require("express");
const app = express();
const logger = require("./logger");
const authorize = require("./authorize");

//if you pass more than one function inside app.use it is executing from left to right.
app.use([logger, authorize]);

app.get("/", (req, res) => {
    return res.send("Home");
});

app.get("/about", (req, res) => {
    return res.send("About");
});

app.get("/api/products", (req, res) => {
    return res.send("Products");
});

app.get("/api/items", (req, res) => {
    return res.send("Items");
});

app.listen(5000, (err) => {
    if (err) {
        console.log("Error : ", err);
        return;
    }
    console.log("Server is listening on port 5000");
});

const express = require("express");
const app = express();

app.get("/", (req, res) => {
    return res.status(200).send("Home page");
});

app.get("/about", (req, res) => {
    return res.status(200).send("About Page");
});

app.all("*", (req, res) => {
    return res.status(404).send("Oops! Resource Not Found!");
});

app.listen(5000, (err) => {
    if (err) {
        console.log("Error : ", err);
        return;
    }
    console.log(`Server is listening on port : 5000`);
});

// app.get
// app.post
// app.delete
// app.put
// app.use
// app.all
// app.listen

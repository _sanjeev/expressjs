const express = require("express");
const app = express();

//req => middleware => res

const logger = (req, res, next) => {
    const method = req.method;
    const url = req.url;
    const time = new Date();
    console.log(method, url, time);
    next();
};

app.get("/", logger, (req, res) => {
    return res.send("Home");
});

app.get("/about", logger, (req, res) => {
    return res.send("About");
});

app.listen(5000, (err) => {
    if (err) {
        console.log("Error : ", err);
        return;
    }
    console.log("Server is listening on port 5000");
});

const express = require("express");
const app = express();

const logger = require("./logger");
const authorize = require("./authorize");

app.get("/", (req, res) => {
    return res.send("Home");
});

app.get("/about", (req, res) => {
    return res.send("About");
});

app.get("/api/products", (req, res) => {
    return res.send("Products");
});

//if you want to pass more than one middleware in a request you can use it like this
app.get("/api/items", [logger, authorize], (req, res) => {
    return res.send("Items");
});

app.listen(5000, (err) => {
    if (err) {
        console.log("Error : ", err);
        return;
    }
    console.log("Server is listening on port : 5000");
});

const express = require("express");
const app = express();

const { products, people } = require("./data");

app.get("/", (req, res) => {
    return res.send("<h1>Home Page</h1> <a href='/api/products'>Go to products</a>");
});

app.get("/api/products", (req, res) => {
    const newProducts = products.map((product) => {
        const { id, name, image } = product;
        return {
            id,
            name,
            image,
        };
    });
    return res.status(200).json(newProducts);
});

app.get("/api/products/:id", (req, res) => {
    const { id } = req.params;
    const product = products.find((product) => product.id === Number(id));
    if (product) return res.status(200).json(product);
    return res.status(404).send("<h1>Oops! resource not found!</h1>");
});

app.get("/api/products/:productId/reviews/:reviewId", (req, res) => {
    return res.send("<h1>Hello World</h1>");
});

app.listen(5000, (err) => {
    if (err) {
        console.log("Error : ", err);
        return;
    }
    console.log("Server is listening on port : 5000");
});

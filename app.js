const express = require("express");
const app = express();
let { people } = require("./data");
const bodyParser = require("body-parser");
const peopleRouter = require("./routes/people");
const auth = require("./routes/auth");
//parse form data
app.use(express.urlencoded({ extended: false }));
//parse json data
app.use(express.json());
//base route
app.use("/api/people", peopleRouter);
//base route
app.use("/login", auth);

app.listen(5000, (err) => {
    if (err) {
        console.log("Error : ", err);
        return;
    }
    console.log("Server is listening on port 5000");
});
